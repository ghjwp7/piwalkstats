.. -*- mode: rst -*-
..  To view this as a local file in browser, use `restview README.rst`
..  Browser page will update whenever a changed version is stored.

===============
piWalkStats
===============

Project Contents:
------------------

• 26087.msg and 26090.msg — text files with sci.math messages about
  using digits of pi as step-lengths when tracing out a walk in the XY
  plane
• `drawExtremes1.jl` to produce separate
  plots of X and Y statistics for given numbers of digits of pi
• `piWalk1.jl` for basic statistics in digits-of-pi walks, where
  directions D,L,U,R,D,R,U,L are taken in turn, with lengths dictated
  by digit values
• `spinner1.jl`, to produce stats for pi et al spin-walks, where
  directions 0°, 36°, ... 324° are taken in turn, with lengths
  dictated by digit values; with first walk for pi, others for random
  sequences
• `variedExtremes1.jl`, to process digits of π, ℯ, √2, and random
  sequences, then output a picture of 4 plots representing Xmin, Xmax,
  Ymin, and Ymax for each of the digit sources
• `writeExtremes1.jl` (obsolete) to process digits of pi and write
  statistics change data to output
• `xyPlot1.jl`, to draw plots of pi digit walks
• `zeroMillions.jl`, which tracks number of returns-to-origin, by
  default for 200 ea. million-digit random numbers, for two kinds of walks
• plot outputs `walkPlot100.png` – `walkPlot10000000.png` produced by
  `./xyPlot1.jl 100` – `./xyPlot1.jl 10000000`
• plot output `qdata1000000000.png` from `./variedExtremes1.jl
  1000000000` which processed a billion digits each of π, ℯ, √2, and
  half a dozen random sequences, during a 2.5 hour run. [For a given
  number of digits, the wide traces — for π, ℯ, √2 — retain their
  shapes from run to run, while the thin traces — for lengthy random
  numbers — change with every run.]
• `results-piWalk1`, with text outputs from runs of above programs

**Text output from** `./variedExtremes1.jl 1000000000` **includes** ::

              Xmin      Xmax     Cross       Xon      Ymin      Ymax     Cross       Yon
  π         -46684     20240     58685     29687   -183016     12484      6953      3590
  ℯ         -27304     55228     22454     11111   -115968      3978      1672      1018
  √2        -12630    129896      9449      4801    -23745     59017     25091     14110
  r1        -46215     33313     27473     13787    -53016     37639     28064     15378
  r2         -5825    113530     17036      8363    -49907     76941     15364      8698
  r3       -149105      1616      3148      1535    -62420     28075     52512     28656
  r4        -29264     65977     34589     17503     -6018    114719     10315      5794
  r5          -257     69932       751       357    -94808     12544     15174      8458
  r6        -31333     32389     40965     20751    -10189     73012     15771      8692
  real  152m7.218s

The table above represents the extreme excursions of billion-digit XY
walks for eight different real numbers, each expressed to over a
billion digits.  (For every-digit-accuracy, `variedExtremes1` adds a
buffer of 66 not-processed digits at the ends of numbers.  While a
smaller extension might work, specifying just one or two digits extra
didn't.)  The associated plot, `qdata1000000000.png`, portrays the
relative progress toward each of those final values, as digit number
increases, rather than just giving final, overall values.

The table also reports, via Xon and Yon statistics, exact number of
times each walk landed exactly on the X or Y axis.  The Cross values
give lower bounds for the numbers of crossings of X and Y axes, ie,
are highly accurate approximations but likely to be inexact, as
explained in one of the notes below.

References:
------------------

[1]  https://en.wikipedia.org/wiki/Random_walk

[2]  https://en.wikipedia.org/wiki/Normal_number


Notes:
------------------

The `.jl` files are Julia-language programs that can be run by
`./programname` on typical linux systems, or by `julia programname`
otherwise.

As can be seen in the programs and the .msg files, base-10 digits of a
number (eg, π) are used as step-lengths as we trace out a walk in the
XY plane, taking different directions in turn.  Suppose our number is
simply normal – ie all digits and digit sequences of equal likelihood,
per [2] – and further suppose we start from (0,0) and take steps
without directional bias.  Under such assumptions we expect that in
the long run Xmin, Xmax, Ymin, and Ymax will all be of the same order
of magnitude.  Generally, there's no finite bound on how distant from
the origin the trace may go.

In the usual random walk on the plane, unit steps are taken in random
directions on the XY lattice.  See [1] which points out that for
random walks with `n` unit steps, the spatial breadth of the walk is
in the long run proportional to `√n`.

In the present case, we cycle through directions in a pattern of
length 8, (D,L,U,R,D,R,U,L).  As Ups, Downs, Lefts, and Rights balance
out in each cycle, there is no directional bias in the long run.

In output plots, the X axis represents digit number – that is, 1, 2,
3... for successive digits of an input value. For example, if the
input value is π (pi) or about 3.14159265..., then digits 1, 2, 3, 4
are '3', '1', '4', '1'.  The Y axis represents a scaled value of the
statistic being plotted – any of Xmin, Xmax, XCross, Xon, Ymin, Ymax,
YCross, Yon – as of a given digit number.  Thus, a long flat line
represents a statistic that did not change during a long stretch of
digits.  Each vertical line in a plot represents a statistic changing
in value at a given digit.

The scale factor for each trace in a plot is `1/lv`, where `lv` is the
last value taken by a statistic during a walk.  For example, if an
Xmax statistic gradually increases up to 12345 as digits are
processed, but no further than that, then the scale factor for that
Xmax statistic will be 1/12345.  Each trace is individually scaled.


**Cross is inexact –** Whenever more than two '0' digits occur
consecutively at the same time the trace is on an axis, the programs
may miss an axis crossing, because of retaining only a four-step
travel history as the walk progresses.  In a billion simply-normal
digits (see [2]) we can expect on the order of 1111111 occurrences of
three or more consecutive '0's.  For pi, the trace was on axis 33277
times (ie 29687+3590), an average of about once in 30051 steps.  If
long strings of '0's are uniformly randomly distributed and if on-axis
instances are uniformly randomly distributed as well, we expect a long
string of '0's to have a chance of ~1 in 30051 of colliding with an
on-axis instance, so the Cross values for pi are probably low by a
number like 1111111/30051, or about 37, which would be about 1 error
per 1774 crossings.  A similar calculation for the r2 walk (on a
billion random digits) yields a value of about 19, which would be
about 1 error per 1705 crossings.  In short, while the Cross values
are merely lower bounds rather than exact, it looks like the amount of
error is negligible.  Note, a `d`-digit string of '0's results in an
error only when the first '0' of the string coincides with being
on-axis, giving the calculation as above, rather than a value `d`
times bigger.  Note, in a future version, it should be possible to
make handle the recent-trace history using an array of say 20 items
(instead of current method with 4 separate variable), plus tests for
accuracy by counting numbers of more than that many '0's in a row.

**For comparison,** here are the first 40 coordinates that piWalk1
visits, followed by the first 40 coordinates that spinner1 visits.   ::

  zili ~/stpmd/piwalkstats > ./piWalk1.jl 40 40
  Got pi to 103 digits, will process 40
  3.14159265358979323846264338327950288419...
     1.  3 D  x=  0  y= -3
     2.  1 L  x= -1  y= -3
     3.  4 U  x= -1  y=  1
     4.  1 R  x=  0  y=  1
     5.  5 D  x=  0  y= -4
     6.  9 R  x=  9  y= -4
     7.  2 U  x=  9  y= -2
     8.  6 L  x=  3  y= -2
     9.  5 D  x=  3  y= -7
    10.  3 L  x=  0  y= -7
    11.  5 U  x=  0  y= -2
    12.  8 R  x=  8  y= -2
    13.  9 D  x=  8  y=-11
    14.  7 R  x= 15  y=-11
    15.  9 U  x= 15  y= -2
    16.  3 L  x= 12  y= -2
    17.  2 D  x= 12  y= -4
    18.  3 L  x=  9  y= -4
    19.  8 U  x=  9  y=  4
    20.  4 R  x= 13  y=  4
    21.  6 D  x= 13  y= -2
    22.  2 R  x= 15  y= -2
    23.  6 U  x= 15  y=  4
    24.  4 L  x= 11  y=  4
    25.  3 D  x= 11  y=  1
    26.  3 L  x=  8  y=  1
    27.  8 U  x=  8  y=  9
    28.  3 R  x= 11  y=  9
    29.  2 D  x= 11  y=  7
    30.  7 R  x= 18  y=  7
    31.  9 U  x= 18  y= 16
    32.  5 L  x= 13  y= 16
    33.  0 D  x= 13  y= 16
    34.  2 L  x= 11  y= 16
    35.  8 U  x= 11  y= 24
    36.  8 R  x= 19  y= 24
    37.  4 D  x= 19  y= 20
    38.  1 R  x= 20  y= 20
    39.  9 U  x= 20  y= 29
    40.  7 L  x= 13  y= 29
        Min     Max   Cross    Zero     Last       @Orig
  X:     -1      20       1       5       13       0  #0
  Y:    -11      29       5       0       29
  
  zili ~/stpmd/piwalkstats > ./spinner1.jl 1 40 40
      D#  Dig.     X       Y         dx      dy    #digits=40
       1.  3     3.00    0.00       3.00    0.00
       2.  1     3.81    0.59       0.81    0.59
       3.  4     5.05    4.39       1.24    3.80
       4.  1     4.74    5.34      -0.31    0.95
       5.  5     0.69    8.28      -4.05    2.94
       6.  9    -8.31    8.28      -9.00    0.00
       7.  2    -9.93    7.11      -1.62   -1.18
       8.  6   -11.78    1.40      -1.85   -5.71
       9.  5   -10.24   -3.36       1.55   -4.76
      10.  3    -7.81   -5.12       2.43   -1.76
      11.  5    -2.81   -5.12       5.00    0.00
      12.  8     3.66   -0.42       6.47    4.70
      13.  9     6.44    8.14       2.78    8.56
      14.  7     4.28   14.80      -2.16    6.66
      15.  9    -3.00   20.09      -7.28    5.29
      16.  3    -6.00   20.09      -3.00    0.00
      17.  2    -7.62   18.92      -1.62   -1.18
      18.  3    -8.55   16.06      -0.93   -2.85
      19.  8    -6.07    8.45       2.47   -7.61
      20.  4    -2.84    6.10       3.24   -2.35
      21.  6     3.16    6.10       6.00    0.00
      22.  2     4.78    7.28       1.62    1.18
      23.  6     6.64   12.98       1.85    5.71
      24.  4     5.40   16.79      -1.24    3.80
      25.  3     2.97   18.55      -2.43    1.76
      26.  3    -0.03   18.55      -3.00    0.00
      27.  8    -6.50   13.85      -6.47   -4.70
      28.  3    -7.43   11.00      -0.93   -2.85
      29.  2    -6.81    9.09       0.62   -1.90
      30.  7    -1.15    4.98       5.66   -4.11
      31.  9     7.85    4.98       9.00    0.00
      32.  5    11.90    7.92       4.05    2.94
      33.  0    11.90    7.92       0.00    0.00
      34.  2    11.28    9.82      -0.62    1.90
      35.  8     4.81   14.52      -6.47    4.70
      36.  8    -3.19   14.52      -8.00    0.00
      37.  4    -6.43   12.17      -3.24   -2.35
      38.  1    -6.74   11.22      -0.31   -0.95
      39.  9    -3.95    2.66       2.78   -8.56
      40.  7     1.71   -1.45       5.66   -4.11
     NearZZ   NearX    NearY    Xmin    Xmax      Ymin    Ymax     CrossX   CrossY
         0        0        1   -11.78    11.90    -5.12    20.09        8        3
