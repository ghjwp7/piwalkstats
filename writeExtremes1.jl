#!/usr/bin/env julia
# -*- mode: julia; -*- 

# ~ jiw ~ 16 April 2023 ~  ~
# Subject: Pi and its digits on the Cartesian coordinate plain.
# From: Dan joyce <danj4084@gmail.com>
# Newsgroups: sci.math
# Date: Sun, 16 Apr 2023 14:00:00 -0700 (PDT)

module writeExtremes
# This program writes data to stdout, recording change points in
# extremes, along with zero and crossover counts, of the sort detected
# by piwalk1.  For example, when processing 1000000 digits of pi,
# piwalk1 reports:
#       Min     Max   Cross    Zero     Last       @Orig
# X:  -2076     439    1074     563    -1648       1  #54073
# Y:   -752    1842     753     388     -574

# In this version, whenever a new min or max is found, we write a
# record with Min, Max, Cross, and Zero data for X and for Y.  Params
# shoCross and shoZero control if crossings and zeros also trigger
# data output.

using Formatting

function walker(ndigi, ps, oop, shoCross, shoZero)
    # Do digits-walk for ps while recording trace stats including x/y
    # min/max, and axis-on & axis-crossing counts whenever x/y min/max
    # change.  End after ndigi digits or after outputting oop data
    # records.
    D,L,U,R = ((0,-1), (-1,0), (0,1), (1,0))
    appOrder = (D,L,U,R,D,R,U,L) # Order of applying moves
    xmin = xmax = ymin = ymax = outCount = 0
    xon = yon = xover = yover = atzero = lastZZ = 0
    px = py = qx = qy = rx = ry = sx = sy = 0
    dat = dout = dirt = 0
    limi = min(ndigi, length(ps))
    while dout < limi
        dat += 1
        c = ps[dat]
        if c != '.'
            dirt += 1            # Go to next direction
            if dirt > length(appOrder)
                dirt = 1
            end
            dx, dy = appOrder[dirt]
            v = Int(c) & 15
            qx, qy, rx, ry, sx, sy = px, py, qx, qy, rx, ry 
            px, py =  px + v*dx,  py + v*dy
            newMM = false
            if px < xmin;  xmin = px;  newMM = true; end
            if px > xmax;  xmax = px;  newMM = true; end
            if py < ymin;  ymin = py;  newMM = true; end
            if py > ymax;  ymax = py;  newMM = true; end
            # This can detect ok when staying on axis only for up to 2
            # 0-digits in a row (like, px goes -1, 0, 0, 1 is ok)
            if px==0
                if py==0        # At origin
                    atzero += 1;  lastZZ = dout+1
                end
                xon += 1        # On x-axis
                shoZero && (newMM = true)
            else                # px is off-axis
                if px*qx < 0 ||
                    (qx==0 && ((px*rx < 0) || (rx==0 && px*sx < 0)))
                    xover += 1
                    shoCross && (newMM = true)
                end
            end

            if py==0
                yon += 1        # On y-axis
                shoZero && (newMM = true)
            else                # py is off-axis
                if py*qy < 0 ||
                    (qy==0 && ((py*ry < 0) || (ry==0 && py*sy < 0)))
                    yover += 1
                    shoCross && (newMM = true)
                end
            end
            dout += 1
            if newMM
                outCount += 1
                printfmtln("{:7d} {:7d} {:7d} {:7d} {:7d} {:7d} {:7d} {:7d} {:7d}",
                    dout, xmin, ymin, xmax, ymax, xover, yover, xon, yon)
                outCount ≥ oop && return   # We done here?
            end
        end
    end
end

function maimai()
    arn = 0;        nargs = length(ARGS)
    # Get parameter `ndigi`, number of decimal places of pi
    arn+=1; ndigi = nargs>=arn ? parse(Int64, ARGS[arn]) : 10000
    arn+=1; oop   = nargs>=arn ? parse(Int64, ARGS[arn]) : ndigi
    p = BigFloat(π, precision=200+Int(round(ndigi/0.301030)))
    s = string(p)
    #println("Got pi to $(length(s)) digits, will process $ndigi with <$oop reports")
    #println("$(s[1:min(66, ndigi)])...")
    # Get stats of digits-walk for p
    walker(ndigi, s, oop, true, true)
end
maimai()
end # module
