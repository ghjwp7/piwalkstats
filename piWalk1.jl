#!/usr/bin/env julia
# -*- mode: julia; -*- 

# ~ jiw ~ 16 April 2023 ~  ~
# Subject: Pi and its digits on the Cartesian coordinate plain.
# From: Dan joyce <danj4084@gmail.com>
# Newsgroups: sci.math
# Date: Sun, 16 Apr 2023 14:00:00 -0700 (PDT)

module smallbit
using Formatting

function walker(ndigi, ps, oop)
    # Do digits-walk for ps while accumulating trace stats including
    # x/y min/max, axis-on & axis-crossing counts, and at-origin counts.
    # Print first oop coordinate pairs that we encounter.
    D,L,U,R = ((0,-1), (-1,0), (0,1), (1,0))
    appOrder = (D,L,U,R,D,R,U,L) # Order of applying moves
    xmin = xmax = ymin = ymax = 0
    xon = yon = xover = yover = atzero = lastZZ = 0
    px = py = qx = qy = rx = ry = sx = sy = 0
    dat = dout = dirt = 0
    limi = min(ndigi, length(ps))
    while dout < limi
        dat += 1
        c = ps[dat]
        if c != '.'
            dirt += 1            # Go to next direction
            if dirt > length(appOrder)
                dirt = 1
            end
            dx, dy = appOrder[dirt]
            v = Int(c) & 15
            qx, qy, rx, ry, sx, sy = px, py, qx, qy, rx, ry 
            px, py =  px + v*dx,  py + v*dy
            xmin, xmax = min(xmin, px), max(xmax, px)
            ymin, ymax = min(ymin, py), max(ymax, py)

            # This can detect ok when staying on axis only for up to 2
            # 0-digits in a row (like, px goes -1, 0, 0, 1 is ok)
            if px==0
                if py==0        # At origin
                    atzero += 1;  lastZZ = dout+1
                end
                xon += 1        # On x-axis
            else                # px is off-axis
                if px*qx < 0 ||
                    (qx==0 && ((px*rx < 0) || (rx==0 && px*sx < 0)))
                    xover += 1
                end
            end

            if py==0
                yon += 1        # On y-axis
            else                # py is off-axis
                if py*qy < 0 ||
                    (qy==0 && ((py*ry < 0) || (ry==0 && py*sy < 0)))
                    yover += 1
                end
            end

            if dout < oop
                printfmtln("{:4d}.  {} {}  x={:3d}  y={:3d}",
                           dout+1, c, "DLURDRUL"[dirt], px, py)
            end
            dout += 1
        end
    end
    println(   "      Min     Max   Cross    Zero     Last       @Orig")
    printfmtln("X: {:6d}  {:6d}  {:6d}   {:5d}   {:6d}   {:5d}  #{}",
               xmin, xmax, xover, xon, px, atzero, lastZZ)
    printfmtln("Y: {:6d}  {:6d}  {:6d}   {:5d}   {:6d}",
               ymin, ymax, yover, yon, py)
end

function maimai()
    arn = 0;        nargs = length(ARGS)
    # Get parameter `ndigi`, number of decimal places of pi
    arn+=1; ndigi = nargs>=arn ? parse(Int64, ARGS[arn]) : 100
    arn+=1; oop   = nargs>=arn ? parse(Int64, ARGS[arn]) : 0
    p = BigFloat(π, precision=200+Int(round(ndigi/0.301030)))
    s = string(p)
    println("Got pi to $(length(s)) digits, will process $ndigi")
    println("$(s[1:min(66, ndigi)])...")
    # Get stats of digits-walk for p
    walker(ndigi, s, oop)
end
maimai()
end # module
