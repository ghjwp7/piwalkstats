#!/usr/bin/env julia
# -*- mode: julia; -*- 

# ~ jiw ~ 16 April 2023 ~  ~
# Subject: Pi and its digits on the Cartesian coordinate plain.
# From: Dan joyce <danj4084@gmail.com>
# Newsgroups: sci.math
# Date: Sun, 16 Apr 2023 14:00:00 -0700 (PDT)

module drawExtremes
# This program records change points in extremes, along with zero and
# crossover counts, of the sort detected by piwalk1.  For example,
# when processing 1000000 digits of pi, piwalk1 reports:

#       Min     Max   Cross    Zero     Last       @Orig
# X:  -2076     439    1074     563    -1648       1  #54073
# Y:   -752    1842     753     388     -574

# This version maintains 8 pairs of arrays, one pair for each of the
# Min, Max, Cross, and Zero data for X and for Y statistics.  Whenever
# a new min/max/crossing/zero is found, in the array pair for the
# changed statistic we add entries with current digit number and the
# new statistic value.  Later, after all digits are processed, we make
# plots of data saved in arrays.

using Plots
using Formatting

function walker(ndigi, ps)
    # Do digits-walk for ps while recording stats including x/y
    # min/max, and axis-on & axis-crossing counts whenever they
    # change.
    function addit(npair, value)
        pair = stats[npair]
        push!(pair[1], 1+dout)    # Digit number (1,2,3...)
        push!(pair[2], value)
    end
    stats = (([0],[0]), ([0],[0]), ([0],[0]), ([0],[0]),  # X stats
             ([0],[0]), ([0],[0]), ([0],[0]), ([0],[0]))  # Y stats
    D,L,U,R = ((0,-1), (-1,0), (0,1), (1,0))
    appOrder = (D,L,U,R,D,R,U,L) # Order of applying moves
    xmin = xmax = ymin = ymax = outCount = 0
    xon = yon = xover = yover = atzero = lastZZ = 0
    px = py = qx = qy = rx = ry = sx = sy = 0
    dat = dout = dirt = 0
    limi = min(ndigi, length(ps))
    while dout < limi
        dat += 1
        c = ps[dat]
        if c != '.'
            dirt += 1            # Go to next direction
            if dirt > length(appOrder)
                dirt = 1
            end
            dx, dy = appOrder[dirt]
            v = Int(c) & 15
            qx, qy, rx, ry, sx, sy = px, py, qx, qy, rx, ry 
            px, py =  px + v*dx,  py + v*dy
            newMM = false
            if px < xmin;  xmin = px;  addit(1, xmin); end
            if px > xmax;  xmax = px;  addit(2, xmax); end
            if py < ymin;  ymin = py;  addit(5, ymin); end
            if py > ymax;  ymax = py;  addit(6, ymax); end
            # This can detect ok when staying on axis only for up to 2
            # 0-digits in a row (like, px goes -1, 0, 0, 1 is ok)
            if px==0
                if py==0        # At origin
                    atzero += 1;  lastZZ = dout+1
                end
                xon += 1        # On x-axis
                addit(4, xon)
            else                # px is off-axis
                if px*qx < 0 ||
                    (qx==0 && ((px*rx < 0) || (rx==0 && px*sx < 0)))
                    xover += 1
                    addit(3, xover)
                end
            end

            if py==0
                yon += 1        # On y-axis
                addit(8, yon)
            else                # py is off-axis
                if py*qy < 0 ||
                    (qy==0 && ((py*ry < 0) || (ry==0 && py*sy < 0)))
                    yover += 1
                    addit(7, yover)
                end
            end
            dout += 1
        end
    end
    return stats
end

function testframe(ndigi)
    t0 = time()
    p = BigFloat(π, precision=200+Int(round(ndigi/0.301030)))
    t1 = time()
    s = string(p)
    # Get stats of digits-walk for p
    t2 = time()
    stats = walker(ndigi, s)
    t3 = time()
    printfmtln("ndigi={:11}  tDelta=  {:13.9f}  {:13.9f}  {:13.9f}   tTotal= {:14.9f}",
               ndigi, t1-t0, t2-t1, t3-t2, t3-t0)
    return stats
end

function maimai()
    stats = (([0],[0]),)
    arn = 0;        nargs = length(ARGS)
    # Get parameter `ndigi`, number of decimal places of pi
    arn+=1; ndigi = nargs>=arn ? parse(Int64, ARGS[arn]) : 500000
    arn+=1; oop   = nargs>=arn ? parse(Int64, ARGS[arn]) : 1
    println("          Delta times are:    compute pi     make string     make stats")
    for k in 1:oop
        stats = testframe(ndigi)
        println("Stat sizes\n     Xmin     Xmax    Cross      Xon     Ymin     Ymax    Cross      Yon")
        for (xs,ys) in stats
            printfmt("{:9d}", length(ys))
        end
        println()
        function plotXorY(statspart, desi)
            p = plot()
            for (xs,ys) in statspart
                hival = ys[end]
                #vscale = 1/(hival^2)
                #yss = map(x -> vscale * x^2, ys)
                vscale = 1/hival
                yss = map(x -> vscale * x, ys)
                plot!(p, xs, yss, lw=2, size=(1700,933))
            end
            return p
        end
        px = plotXorY(stats[1:4], "X")
        py = plotXorY(stats[5:8], "Y")
        plot(px, py, layout=(2,1))
        savefig("outdata$(fmt("010d", ndigi)).png")
        ndigi += ndigi
    end
    
    if oop > 1
        println("          Delta times are:    compute pi     make string     make stats")
    end
end
maimai()
end # module
