#!/usr/bin/env julia
# -*- mode: julia; -*- 

# ~ jiw ~ 16 April 2023 ~  ~
# Subject: Pi and its digits on the Cartesian coordinate plain.
# From: Dan joyce <danj4084@gmail.com>
# Newsgroups: sci.math
# Date: Sun, 16 Apr 2023 14:00:00 -0700 (PDT)

module drawExtremes
# This program records change points in extremes, along with zero and
# crossover counts, of the sort detected by piwalk1.  For example,
# when processing 1000000 digits of pi, piwalk1 reports:

#       Min     Max   Cross    Zero     Last       @Orig
# X:  -2076     439    1074     563    -1648       1  #54073
# Y:   -752    1842     753     388     -574

# This version maintains 8 pairs of arrays, one pair for each of the
# Min, Max, Cross, and Zero data for X and for Y statistics.  Whenever
# a new min/max/crossing/zero is found, in the array pair for the
# changed statistic we add entries with current digit number and the
# new statistic value.

# Previous program drawExtremes1 made plots of all stats data saved in
# arrays, after all digits were processed.  Instead of that, this
# program (variedExtremes1) plots just min and max data, but does so
# for several different sources of digits:  pi, 

using Plots
using Formatting

function walker(ndigi, ps)
    # Given an input string of digits of a real number, walk the
    # digits while accumulating min/max/cross/zero stats.  Make an
    # array entry whenever any of the following changes: x/y min/max,
    # x/y axis-on count, x/y axis-crossing count.

    # Return a tuple of 8 pairs of arrays, with 4 pairs for X stats
    # and 4 pairs for Y stats.
    function addit(npair, value)
        pair = stats[npair]
        push!(pair[1], 1+dout)    # Digit number (1,2,3...)
        push!(pair[2], value)
    end
    stats = (([0],[0]), ([0],[0]), ([0],[0]), ([0],[0]),  # X stats
             ([0],[0]), ([0],[0]), ([0],[0]), ([0],[0]))  # Y stats
    D,L,U,R = ((0,-1), (-1,0), (0,1), (1,0))
    appOrder = (D,L,U,R,D,R,U,L) # Order of applying moves
    xmin = xmax = ymin = ymax = outCount = 0
    xon = yon = xover = yover = atzero = lastZZ = 0
    px = py = qx = qy = rx = ry = sx = sy = 0
    dat = dout = dirt = 0
    limi = min(ndigi, length(ps))
    while dout < limi
        dat += 1
        c = ps[dat]
        if c != '.'
            dirt += 1            # Go to next direction
            if dirt > length(appOrder)
                dirt = 1
            end
            dx, dy = appOrder[dirt]
            v = Int(c) & 15
            qx, qy, rx, ry, sx, sy = px, py, qx, qy, rx, ry 
            px, py =  px + v*dx,  py + v*dy
            newMM = false
            if px < xmin;  xmin = px;  addit(1, xmin); end
            if px > xmax;  xmax = px;  addit(2, xmax); end
            if py < ymin;  ymin = py;  addit(5, ymin); end
            if py > ymax;  ymax = py;  addit(6, ymax); end
            # This can detect ok when staying on axis only for up to 2
            # 0-digits in a row (like, px goes -1, 0, 0, 1 is ok)
            if px==0
                if py==0        # At origin
                    atzero += 1;  lastZZ = dout+1
                end
                xon += 1        # On x-axis
                addit(4, xon)
            else                # px is off-axis
                if px*qx < 0 ||
                    (qx==0 && ((px*rx < 0) || (rx==0 && px*sx < 0)))
                    xover += 1
                    addit(3, xover)
                end
            end

            if py==0
                yon += 1        # On y-axis
                addit(8, yon)
            else                # py is off-axis
                if py*qy < 0 ||
                    (qy==0 && ((py*ry < 0) || (ry==0 && py*sy < 0)))
                    yover += 1
                    addit(7, yover)
                end
            end
            dout += 1
        end
    end
    return stats
end

function maimai()
    stats = (([0],[0]),)
    arn = 0;        nargs = length(ARGS)
    # Get parameter `ndigi`, number of decimal places of pi
    arn+=1; ndigi   = nargs>=arn ? parse(Int64, ARGS[arn]) : 1000000
    arn+=1; dataPow = nargs>=arn ? parse(Int64, ARGS[arn]) : 1

    setprecision(ndigi+66; base=10)
    sorcNames   = ("π", "ℯ", "√2", "r1", "r2", "r3", "r4", "r5", "r6")
    sorcNumbers = (BigFloat(π),    BigFloat(ℯ),    √(BigFloat(2)),
                   rand(BigFloat), rand(BigFloat), rand(BigFloat),
                   rand(BigFloat), rand(BigFloat), rand(BigFloat))
    sorcStrings = (string(x) for x in sorcNumbers)
    statset = []
    println("Stat values\n            Xmin      Xmax     Cross       Xon      Ymin      Ymax     Cross       Yon")
    for (dt, sn) in zip(sorcStrings, sorcNames)
        stats = walker(ndigi, dt)
        printfmt("{:5s} ", sn)
        for (xs,ys) in stats
            #printfmt("{:9d}", length(ys))
            printfmt("{:10d}", ys[end])
        end
        println()
        push!(statset, stats)
    end
    #  To plot one stat as drawn from several sources
    function plotStat(statnum)
        statNames=("Xmin", "Xmax", "Xcross", "Xon", "Ymin", "Ymax", "Ycross", "Yon")
        p = plot()
        print("Drawing... ")
        for k in 1:length(sorcNames)
            (xs,ys) = statset[k][statnum]
            hival = ys[end]
            vscale = 1/hival
            yss = map(v -> (vscale * v)^dataPow, ys)
            plot!(p, xs, yss, lw=k<4 ? 5 : 2, label=sorcNames[k], 
                  size=(1700,933), title=statNames[statnum])
            print(" $(length(xs))")
        end
        println(" points for stat# $statnum")
        return p
    end
    #  Make plots of various stats, across the several sources
    pxl = plotStat(1)
    pxh = plotStat(2)
    pyl = plotStat(5)
    pyh = plotStat(6)
    plot(pxl, pxh, pyl, pyh, layout=(2,2))
    savefig("qdata$(fmt("010d", ndigi)).png")
    ndigi += ndigi
end
maimai()
end # module
