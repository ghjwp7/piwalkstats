#!/usr/bin/env julia
# -*- mode: julia; -*- 

# ~ jiw ~ 16 April 2023 ~  ~
# Subject: Pi and its digits on the Cartesian coordinate plain.
# From: Dan joyce <danj4084@gmail.com>
# Newsgroups: sci.math
# Date: Sun, 16 Apr 2023 14:00:00 -0700 (PDT)

module spinner
# Spinner1 counts number of near-origin visits and numbers of axis
# crossings, when doing "spin walks" on the XY plane.  Spin walks are
# not as in piwalk1, but instead have steps at directions that cycle
# repeatedly thru the 10 directions 0, 36, 72, ... 324 degrees.  Each
# step length equals its digit value.  Note, piwalk1 walks are on the
# XY integer lattice, while spin walks are on R^2.

using Formatting

function walker(ndigi, ps, repoLim)
    # Do digits-walk for ps while recording stuff
    NDirs = 10;  NDir1 = NDirs-1
    angleStep = 2π/NDirs
    vex = collect((cos(k*angleStep),sin(k*angleStep)) for k in 0:NDir1)
    xmin = xmax = ymin = ymax = 0.0
    xnear = ynear = xover = yover = atzero = lastZZ = 0
    close = 1/500
    px = py = nearZZ = dirnum = 0
    limi = min(2+ndigi, length(ps))
    repoLim > 0 &&
        println("    D#  Dig.     X       Y         dx      dy    #digits=$ndigi")
    for dat in 3:limi
        dirnum = 1+(dirnum%NDirs) # Go to next direction
        c = ps[dat];            v = Int(c) & 15
        qx = px;                qy = py
        dx = v*vex[dirnum][1];  dy = v*vex[dirnum][2]
        px += dx;               py += dy
        dat-2 ≤ repoLim &&
            printfmtln("{:6d}.  {}  {:7.2f} {:7.2f}    {:7.2f} {:7.2f}", dat-2, c, px, py, dx, dy)

#add axis crossing counts and quadrant-pop counts - & possibly octant counts
        abs(px)<close && (xnear += 1)                 # Count near-x-axis
        abs(py)<close && (ynear += 1)                 # Count near-y-axis
        abs(px)<close && abs(py)<close && (nearZZ += 1)   # Count near-origin
        px < xmin && (xmin = px)
        px > xmax && (xmax = px) # Accumulate min and max X and Y
        py < ymin && (ymin = py)
        py > ymax && (ymax = py)
        px * qx < 0 && (xover += 1)  # Count axis crossings
        py * qy < 0 && (yover += 1)
    end
    return nearZZ, xnear, ynear, xmin, xmax, ymin, ymax, xover, yover
end

function maimai()
    #exit()
    arn = 0;        nargs = length(ARGS)
    # Get parameter `ndigi`, number of decimal places of pi
    arn+=1; oop = nargs>=arn ? parse(Int64, ARGS[arn]) : 1
    arn+=1; ndigi = nargs>=arn ? parse(Int64, ARGS[arn]) : 20
    arn+=1; bop = nargs>=arn ? parse(Int64, ARGS[arn]) : ndigi
    setprecision(ndigi+16; base=10)
    matzmax = 9
    matza = zeros(Int64, matzmax)
    matz  = 0;  oop = abs(oop)
    setprecision(ndigi+66; base=10)
    r = BigFloat(π)/10
    # Eg:            0       17       24   -72.18  1109.99   -54.90   977.04      298      290
    colNames="   NearZZ   NearX    NearY    Xmin    Xmax      Ymin    Ymax     CrossX   CrossY"
    bop < 1 && println(colNames)
    for k in 1:oop
        #nzz, xnear, ynear, xmin, xmax, ymin, ymax, xover, yover = walker(ndigi, string(r), bop)
        #printfmtln("{:8d} {:8d} {:8d} {:8.2f} {:8.2f} {:8.2f} {:8.2f} {:8d} {:8d}",
        #           nzz, xnear, ynear, xmin, xmax, ymin, ymax, xover, yover)
        results = walker(ndigi, string(r), bop)
        bop > 0 && println(colNames)
        printfmtln("{:8d} {:8d} {:8d} {:8.2f} {:8.2f} {:8.2f} {:8.2f} {:8d} {:8d}", results...)
        nzz = results[1]
        if nzz<1
            matz += 1
        elseif nzz ≤ matzmax
            matza[nzz] += 1
        else
            matza[end] += 1
        end
        #if k%40 ==0; println("   $matz"); end
        r = rand(BigFloat)
    end
    #pcNZZ = 100matz/oop
    #println("\nRan $bop tests of length $ndigi, got $matz no-return cases\n$pcNZZ% no-return and $(100-pcNZZ)% with return to origin")
    #println("Histo data:  $matza")
    #nzsum = sum(j->j*matza[j], 1:length(matza))
    #println("Weighted average of returns to origin:  $(nzsum/oop) times per walk")
end
maimai()
end # module
