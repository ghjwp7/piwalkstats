#!/usr/bin/env julia
# -*- mode: julia; -*- 

# ~ jiw ~ 16 April 2023 ~  ~
# Subject: Pi and its digits on the Cartesian coordinate plain.
# From: Dan joyce <danj4084@gmail.com>
# Newsgroups: sci.math
# Date: Sun, 16 Apr 2023 14:00:00 -0700 (PDT)

module xyPlot1
# This program plots a walk that's as in piwalk1.
using Plots

function Dwalker(ndigi, ps, kth)
    # Do digits-walk for ps, recording each kth location. Dwalker
    # takes a d-length step for digit d, @ D,L,U,R,D,R,U,L directions.
    D,L,U,R = ((0,-1), (-1,0), (0,1), (1,0))
    appOrder = (D,L,U,R,D,R,U,L) # Order of applying moves
    px = py = dirt = 0
    limi = min(2+ndigi, length(ps))
    xs, ys = [0], [0]
    for dat in 3:limi
        c = ps[dat];      v = Int(c) & 15
        dirt = 1+(dirt%8)            # Go to next direction
        dx, dy = appOrder[dirt]
        px, py = px + v*dx,  py + v*dy
        if dat%kth == 0
            push!(xs, px)
            push!(ys, py)
        end
    end
    return xs, ys
end

function Swalker(ndigi, ps, kth)
    # Do digits-walk for ps, recording each kth location.  Swalker
    # takes a d-length step for digit d, in direction (++k)×36°
    NDirs = 10;  NDir1 = NDirs-1
    angleStep = -2π/NDirs       # minus for clockwise "as he said"
    angleStep = +2π/NDirs       # plus for CCW "as he showed"
    vex = collect((cos(k*angleStep),sin(k*angleStep)) for k in 0:NDir1)
    px = py = dirnum = 0
    limi = min(2+ndigi, length(ps))
    xs, ys = [0.0], [0.0]
    for dat in 3:limi
        dirnum = 1+(dirnum%NDirs) # Go to next direction
        c = ps[dat];            v = Int(c) & 15
        qx = px;                qy = py
        dx = v*vex[dirnum][1];  dy = v*vex[dirnum][2]
        px += dx;               py += dy
        if dat%kth == 0
            push!(xs, px)
            push!(ys, py)
        end
    end
    return xs, ys
end

function Uwalker(ndigi, ps, kth)
    # Do digits-walk for ps, recording each kth location.  Uwalker
    # takes unit-length steps, in direction d×36° for digit d.
    NDirs = 10
    angleStep = +2π/NDirs       # plus for CCW rotations
    vex = collect((cos(k*angleStep),sin(k*angleStep)) for k in 0:(NDirs-1))
    px = py = 0
    limi = min(2+ndigi, length(ps))
    xs, ys = [0.0], [0.0]
    for dat in 3:limi
        c = ps[dat]
        v = 1+(Int(c) & 15)     # julia arrays are 1-based
        px += vex[v][1]
        py += vex[v][2]
        if dat%kth == 0
            push!(xs, px)
            push!(ys, py)
        end
    end
    return xs, ys
end

function takeWalk(walker, wtag, ndigi, rs, kth)
    print("Start walk")
    xs, ys = walker(ndigi, rs, kth)
    xmin = minimum(xs);  xmax = maximum(xs)
    ymin = minimum(ys);  ymax = maximum(ys)
    print("  Start plot")
    plot(size=(1200,1200))
    plot!([xmin,xmax], [0,0], lc=:black, axis=([], false), label=false)
    plot!([0,0], [ymin,ymax], lc=:black, axis=([], false), label=false)
    npoints = length(xs)
    width = 1; alfa =  0.15
    while npoints * 3^width < 3000000
        width += 1;  alfa *= 1.1
    end
    colos = collect(cgrad([:red, :green, :blue], npoints, alpha = alfa))
    plot!(xs, ys, lw=width, lc=colos, axis=([], false), label=false)
    print("  Start savefig: walkPlot$ndigi$wtag.png")
    savefig("walkPlot$ndigi$wtag.png")
    println("  Done")
end

function maimai()
    walkers =  ((Dwalker,'D'), (Swalker,'S'), (Uwalker,'U'))
    arn = 0;        nargs = length(ARGS)
    # Get parameter `ndigi`, number of decimal places of pi
    arn+=1; ndigi = nargs>=arn ? parse(Int64, ARGS[arn]) : 1000000
    arn+=1; kth   = nargs>=arn ? parse(Int64, ARGS[arn]) : 1
    setprecision(ndigi+16; base=10)
    println("Get pi")
    r  = BigFloat(π)/10
    rs = string(r)
    for (walker, wtag) in walkers
        takeWalk(walker, wtag, ndigi, rs, kth)
    end
end
maimai()
end # module
