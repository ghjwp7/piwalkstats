#!/usr/bin/env julia
# -*- mode: julia; -*- 

# ~ jiw ~ 16 April 2023 ~  ~
# Subject: Pi and its digits on the Cartesian coordinate plain.
# From: Dan joyce <danj4084@gmail.com>
# Newsgroups: sci.math
# Date: Sun, 16 Apr 2023 14:00:00 -0700 (PDT)

module zeroMillions
# This program counts number of origin hits, when doing a walk as in piwalk1.

# This v0 version runs a few million-digit random-number
# digit-distance walks, counting the number of times the origin is
# struck.  For a slight speed advantage, we generate randoms of length
# 1000010 and start processing at dat=3, to avoid test for '.'

using Formatting

function talker(ndigi)
    # Do "standard" random walk for ndigi steps, recording number of
    # origin-returns
    px = py = atZZ = 0
    for dat in 1:ndigi
        r = rand(Int)
        px += 2(r&1)-1
        py += (r&2)-1
        if px==0 && py==0        # At origin
            atZZ += 1
        end
    end
    return atZZ
end

function walker(ndigi, ps)
    # Do digits-walk for ps while recording number of origin-returns
    D,L,U,R = ((0,-1), (-1,0), (0,1), (1,0))
    appOrder = (D,L,U,R,D,R,U,L) # Order of applying moves
    px = py = atZZ = 0
    dat = 2; dout = dirt = 0
    limi = min(2+ndigi, length(ps))
    for dat in 3:limi
        c = ps[dat]
        dirt += 1            # Go to next direction
        if dirt > length(appOrder)
            dirt = 1
        end
        dx, dy = appOrder[dirt]
        v = Int(c) & 15
        px, py =  px + v*dx,  py + v*dy
        if px==0 && py==0        # At origin
            atZZ += 1
        end
        dout += 1
    end
    return atZZ
end

function maimai()
    #exit()
    arn = 0;        nargs = length(ARGS)
    # Get parameter `ndigi`, number of decimal places of pi
    arn+=1; bop = oop = nargs>=arn ? parse(Int64, ARGS[arn]) : 200
    arn+=1; ndigi = nargs>=arn ? parse(Int64, ARGS[arn]) : 1000000
    setprecision(ndigi+16; base=10)
    matzmax = 75
    matza = zeros(Int64, matzmax)
    matz  = 0;  oop = abs(oop)
    for k in 1:oop
        if bop>0
            r = rand(BigFloat)
            nzz = walker(ndigi, string(r))
        else
            nzz = talker(ndigi)
        end
        #print("$nzz ")
        if nzz<1
            matz += 1
        elseif nzz ≤ matzmax
            matza[nzz] += 1
        else
            println("\n$nzz exceeds $matzmax\n")
        end
        #if k%40 ==0; println("   $matz"); end
    end
    pcNZZ = 100matz/oop
    println("\nRan $bop tests of length $ndigi, got $matz no-return cases\n$pcNZZ% no-return and $(100-pcNZZ)% with return to origin")
    println("Histo data:  $matza")
    nzsum = sum(j->j*matza[j], 1:length(matza))
    println("Weighted average of returns to origin:  $(nzsum/oop) times per walk")
end
maimai()
end # module
